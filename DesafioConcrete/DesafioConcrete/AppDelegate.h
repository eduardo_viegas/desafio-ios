//
//  AppDelegate.h
//  DesafioConcrete
//
//  Created by Eduardo Viegas on 3/22/15.
//  Copyright (c) 2015 Eduardo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

