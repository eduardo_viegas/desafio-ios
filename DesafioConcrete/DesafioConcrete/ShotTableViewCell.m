//
//  ShotTableViewCell.m
//  DesafioConcrete
//
//  Created by Eduardo Viegas on 3/22/15.
//  Copyright (c) 2015 Eduardo. All rights reserved.
//

#import "ShotTableViewCell.h"

@implementation ShotTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
