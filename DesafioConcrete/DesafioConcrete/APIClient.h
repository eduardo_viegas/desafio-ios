//
//  APIClient.h
//  DesafioConcrete
//
//  Created by Eduardo Viegas on 3/22/15.
//  Copyright (c) 2015 Eduardo. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface APIClient : AFHTTPSessionManager

@end
